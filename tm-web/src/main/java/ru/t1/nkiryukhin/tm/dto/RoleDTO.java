package ru.t1.nkiryukhin.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.nkiryukhin.tm.enumerated.RoleType;
import ru.t1.nkiryukhin.tm.model.User;

import javax.persistence.*;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "web_role")
public final class RoleDTO {

    @Id
    private String id = UUID.randomUUID().toString();

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "role_type")
    @Enumerated(EnumType.STRING)
    private RoleType roleType = RoleType.USUAL;

    @Override
    public String toString() {
        return roleType.name();
    }

}
